// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnArea.generated.h"
//#include "Engine/Classes/Components/BoxComponent.h"

class UBoxComponent;

UCLASS()
class MYPROJECT_API ASpawnArea : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnArea();
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly);
	//UStaticMeshComponent* BoxSpawn;
	UPROPERTY();
	UBoxComponent* BoxComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	FVector RandomPoint();
};
