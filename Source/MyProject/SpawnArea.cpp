// Fill out your copyright notice in the Description page of Project Settings.
#include "Food.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "Components/SceneComponent.h"
#include "SpawnArea.h"
#include "EngineMinimal.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ASpawnArea::ASpawnArea()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//UBoxComponent* 
		BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
}

// Called when the game starts or when spawned
void ASpawnArea::BeginPlay()
{
	Super::BeginPlay();
	
	FVector GetRelativeBox = BoxComponent->GetRelativeTransform().GetLocation();
	
	UE_LOG(LogTemp, Log, TEXT("InverseTransformPosition: %.2f, %.2f, %.2f"), GetRelativeBox.X, GetRelativeBox.Y, GetRelativeBox.Z);

	FVector GetBoxExtent = BoxComponent->GetScaledBoxExtent();
	UE_LOG(LogTemp, Log, TEXT("InverseBoxExtent: %.2f, %.2f, %.2f"), GetBoxExtent.X, GetBoxExtent.Y, GetBoxExtent.Z);

	
	
}

// Called every frame
void ASpawnArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(2.f);
	FVector RandomPointer = RandomPoint();
	UE_LOG(LogTemp, Log, TEXT("Random: %.2f, %.2f, %.2f"), RandomPointer.X, RandomPointer.Y, RandomPointer.Z);

}

FVector ASpawnArea::RandomPoint()
{
	FVector GetRelativeBox = BoxComponent->GetRelativeTransform().GetLocation();
	FVector GetBoxExtent = BoxComponent->GetScaledBoxExtent();
	//AFood* Food;
	//Food->SetActorLocation(UKismetMathLibrary::RandomPointInBoundingBox(GetRelativeBox, GetBoxExtent));
	return UKismetMathLibrary::RandomPointInBoundingBox(GetRelativeBox, GetBoxExtent);
}

