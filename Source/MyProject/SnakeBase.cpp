// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(4);
	Move();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(MovementSpeed);
	Move();
	MovementSpeed = MovementSpeed - 0.005;

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector VectorElem = GetActorLocation() - FVector(SnakeElements.Num() * ElementSize, 0, -100);
		FTransform TransformElem = FTransform(VectorElem);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, TransformElem);
		NewSnakeElem->AttachToActor(this,FAttachmentTransformRules::KeepRelativeTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
			
			MovementSpeed = MovementSpeed + 0.01;
		}
	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	 //float MovementSpeed = ElementSize;

	switch (LastMoveDirection)
	{
	case  EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case  EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case  EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	case  EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	};

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToogleCollision();

	for (int i = SnakeElements.Num()-1;i>0;i--)
	{
		auto CurrentElem = SnakeElements[i];
		auto PrevElem = SnakeElements[i - 1];
		FVector PrevLocation = PrevElem->GetActorLocation();
		CurrentElem->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToogleCollision();
	
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement,AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
			bool bIsFirst = ElemIndex == 0;
			IInteractable* InteractableInterface = Cast<IInteractable>(Other);
			if ( InteractableInterface )
			{
				InteractableInterface->Interact(this,bIsFirst);
				//Other->Destroy();
			}
	}
}

